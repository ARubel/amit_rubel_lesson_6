#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "inputException.h"
#include "Hexagon.h"
#include "Pentagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, ver = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, ver);
	Hexagon hex(nam, col, ver);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape *ptrhex = &hex;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	std::string shapetypestr;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e - pentagon, h - hexagon, x - quit" << std::endl;
		std::cin >> shapetypestr;
		system("CLS");
		if (shapetypestr.length() > 1)
		{
			std::cerr << "Warning - don't try to build more than one shape at a time! " << std::endl;
		}
		shapetype = shapetypestr[0];
		
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					throw inputException();
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'e':
				std::cout << "enter name, color, vertices" << std::endl;
				std::cin >> nam >> col >> ver;
				pent.setName(nam);
				pent.setColor(col);
				pent.setVer(ver);
				ptrpent->draw();
				break;

			case 'h':
				std::cout << "enter name, color, vertices" << std::endl;
				std::cin >> nam >> col >> ver;
				hex.setName(nam);
				hex.setColor(col);
				hex.setVer(ver);
				ptrhex->draw();
				break;
			case 'x':
				x = 'x';
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			//std::cout << "would you like to add another object press any key if not press x" << std::endl;
			//std::cin.get() >> x;
			//std::cin >> x;
			
		}
		catch (inputException e)
		{
			std::cerr << "Ivalid input!" << std::endl;
			std::cin.clear();
			// discard 'bad' character(s)
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception& e) //changed expression from std::exception e (added a & to allow heritage exceptions).
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		//system("pause");
		return 0;
	
}