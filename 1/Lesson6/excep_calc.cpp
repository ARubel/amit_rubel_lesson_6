#include <iostream>

#define FORBIDDEN_VALUE 8200

int add(int a, int b) 
{
	if (a == FORBIDDEN_VALUE || b == FORBIDDEN_VALUE || (a + b) == FORBIDDEN_VALUE)
	{
		throw std::string ("Value is forbidden!");
		return a + b;
	}
	else
	{
		return a + b;
	}
	
	
}

int  multiply(int a, int b) 
{
	int sum = 0;
	for(int i = 0; i < b; i++)
	{
		try
		{
			sum = add(sum, a);
		}
		catch (std::string s)
		{
			throw s;
		}
		
		
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for(int i = 0; i < b; i++) 
	{
		try
		{
			exponent = multiply(exponent, a);
		}
		catch (std::string s)
		{
			throw s;
		}
		
  };
  return exponent;
}

int main(void) 
{
	int a = 8000, b = 1;
	try
	{
		std::cout << add(a, b) << std::endl;
		std::cout << multiply(a, b) << std::endl;
		std::cout << pow(a, b) << std::endl;
	}
	catch (std::string s)
	{
		std::cout << s << std::endl;
	}

	//std::cout << pow(5, 5) << std::endl;
}