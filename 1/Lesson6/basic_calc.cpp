#include <iostream>
#define FORBIDDEN_VALUE 8200
#define ERR_RETURN -1

int add(int a, int b) {
	if (a == FORBIDDEN_VALUE || b == FORBIDDEN_VALUE)
	{
		return FORBIDDEN_VALUE;
	}
	return a + b;
}

int  multiply(int a, int b) {

  int sum = 0;
  if (a == FORBIDDEN_VALUE || b == FORBIDDEN_VALUE)
  {
	  return FORBIDDEN_VALUE;
  }
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
	if (sum == FORBIDDEN_VALUE)
	{
		return FORBIDDEN_VALUE;
	}
  };
  return sum; //changed return expression
}

int  pow(int a, int b) {

  int exponent = 1;
  if (a == FORBIDDEN_VALUE || b == FORBIDDEN_VALUE)
  {
	  return FORBIDDEN_VALUE;
  }
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
	if (exponent == FORBIDDEN_VALUE)
	{
		return FORBIDDEN_VALUE;
	}
  };
  return exponent;
}

//bool err_val = false;

int main(void) 
{
	int a = 0, b = 0, res = 0;
	
	std::cout << "Enter values for the three functions: " << std::endl;
	std::cin >> a;
	std::cin >> b;
	
	res = add(a, b);
	if (res == FORBIDDEN_VALUE)
	{
		std::cout << "An error has occured. " << std::endl;
	}
	else
	{
		std::cout << add(a, b) << std::endl;
	}
	res = multiply(a, b);
	if (res == FORBIDDEN_VALUE)
	{
		std::cout << "An error has occured. " << std::endl;
	}
	else
	{
		std::cout << multiply(a, b) << std::endl;
	}
	res = pow(a, b);
	if (res == FORBIDDEN_VALUE)
	{
		std::cout << "An error has occured. " << std::endl;
	}
	else
	{
		std::cout << pow(a, b) << std::endl;
	}


	
	//std::cout << pow(5, 5) << std::endl;

}